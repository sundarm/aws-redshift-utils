import py_compile
import os
import tarfile
import shutil

artifact=''

def set_artifact():
    global artifact
    artifact = "aws-redshift-utils.tar"

def get_artifact():
    return artifact

def build(path):
    print("Build")
    print("Compiling files in {0}".format(path))
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.py'):
                print("Compiling file : {0}".format(file))
                py_compile.compile(root+"//"+file,root+"//"+file+"c")

def build_cleanup(path):
    print("Build cleanup")
    print("Removing build files from directories.")
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.pyc'):
                print("Removing file : {0}".format(file))
                os.remove(os.path.join(root, file))

def set_env():
    pass

def deploy(build_directory):
    set_env()
    pre_cleanup(build_directory)
    build(build_directory)
    create_artifact(build_directory, get_artifact())
    build_cleanup(build_directory)

def create_artifact(path, tar_name):
    print("Creating artifact : {0}".format(get_artifact()))
    if os.path.exists(tar_name):
        os.remove(tar_name)
    tar_handle = tarfile.open(tar_name, "w:")
    for root, dirs, files in os.walk(path):
        for file in files:
            if not file.endswith('.py'):
                tar_handle.add(os.path.join(root, file))
    tar_handle.close()

def pre_cleanup(path):
    print("Pre Cleanup")
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.pyc'):
                print("Removing file : {0}".format(file))
                os.remove(os.path.join(root, file))

if __name__ == '__main__':
    print(os.path.dirname(__file__))
    build_directory = "aws-redshift-utils"
    set_artifact()
    deploy(build_directory)
