"""
Developed by Sundar Manoharan
Usage: pgsql <subcmd> [<subcmd options>]
Options:
 -? or --help   display this help

This utility is used to invoke psql command line and use the password from the cached ~/.pgpassword file.
"""

usage = """
pgsql is the customized PostgreSQL interactive terminal, to use the cached encrypted password from ~/.pgpassword file.

Usage:
  pgsql [OPTION]... [DBNAME [USERNAME]]

General options:
  -c, --command=COMMAND    run only single command (SQL or internal) and exit
  -d, --dbname=DBNAME      database name to connect to (default: "ebi")
  -f, --file=FILENAME      execute commands from file, then exit
  -l, --list               list available databases, then exit
  -v, --set=, --variable=NAME=VALUE
                           set psql variable NAME to VALUE
                           (e.g., -v ON_ERROR_STOP=1)
  -V, --version            output version information, then exit
  -X, --no-psqlrc          do not read startup file (~/.psqlrc)
  -1 ("one"), --single-transaction
                           execute as a single transaction (if non-interactive)
  -?, --help[=options]     show this help, then exit
      --help=commands      list backslash commands, then exit
      --help=variables     list special variables, then exit

Input and output options:
  -a, --echo-all           echo all input from script
  -b, --echo-errors        echo failed commands
  -e, --echo-queries       echo commands sent to server
  -E, --echo-hidden        display queries that internal commands generate
  -L, --log-file=FILENAME  send session log to file
  -n, --no-readline        disable enhanced command line editing (readline)
  -o, --output=FILENAME    send query results to file (or |pipe)
  -q, --quiet              run quietly (no messages, only query output)
  -s, --single-step        single-step mode (confirm each query)
  -S, --single-line        single-line mode (end of line terminates SQL command)

Output format options:
  -A, --no-align           unaligned table output mode
  -F, --field-separator=STRING
                           field separator for unaligned output (default: "|")
  -H, --html               HTML table output mode
  -P, --pset=VAR[=ARG]     set printing option VAR to ARG (see \pset command)
  -R, --record-separator=STRING
                           record separator for unaligned output (default: newline)
  -t, --tuples-only        print rows only
  -T, --table-attr=TEXT    set HTML table tag attributes (e.g., width, border)
  -x, --expanded           turn on expanded table output
  -z, --field-separator-zero
                           set field separator for unaligned output to zero byte
  -0, --record-separator-zero
                           set record separator for unaligned output to zero byte

Connection options:
  -h, --host=HOSTNAME      database server host or socket directory (default: "local socket")
  -p, --port=PORT          database server port (default: "5432")
  -U, --username=USERNAME  database user name (default: "ebi")

For more information, type "\?" (for internal commands) or "\help" (for SQL
commands) from within psql, or consult the psql section in the PostgreSQL
documentation.

Report bugs to <sundarm@systechusa.com>.
"""


import os
import sys
import re
import string
from Crypto.Cipher import AES
import base64

from security.security import decrypt
from security.security import keyes_key
from utils.utils import check_os
from utils.utils import check_file_exists

password_file=".pgpassword"
user=os.getenv("USER")
home_dir=os.getenv("HOME")
password_file_path=home_dir+"/"+password_file

def get_host(param):
    host = ""
    loop = 1
    while (loop < len(param)):
        if ((param[loop] == "-h") or re.search("--host=*",param[loop].strip())):
            if (param[loop] == "-h"):
                host = param[loop+1]
            else:
                host = param[loop].split("=")[1]
            break
        loop = loop + 1
    return host

def get_userid(param):
    user_id = ""
    loop = 1
    while (loop < len(param)):
        if ((param[loop] == "-U") or re.search("--username=*",param[loop].strip())):
            if (param[loop] == "-U"):
                user_id = param[loop+1]
            else:
                user_id = param[loop].split("=")[1]
            break
        loop = loop + 1
    return user_id


def get_encrypted_password(host, user_id):
    encrypted_password=""
    decrypted_key = ""
    password_file_exists = check_file_exists(password_file_path)
    if password_file_exists is True:
        file = open(password_file_path)
        lines = file.readlines()
        key_metadata = lines[0].split(":")
        ordinal_value = key_metadata[1].rstrip("\n")
        encrypted_key = key_metadata[2].rstrip("\n")
        keyees_key = key_metadata[3].rstrip("\n")
        actual_keyees_key = keyes_key(keyees_key, int(ordinal_value))
        decrypted_key = decrypt(encrypted_key, actual_keyees_key)
        for line in range(1,len(lines)):
            cached_password_entry = lines[line].split(":")
            cached_host = str(cached_password_entry[0].rstrip("\n"))
            cached_user = str(cached_password_entry[1].rstrip("\n"))
            cached_encrypted_password = str(cached_password_entry[2].rstrip("\n"))
            if (host == cached_host and user_id == cached_user):
                encrypted_password = cached_encrypted_password
                break
        file.close()
    else:
        encrypted_password = ""
    return encrypted_password, decrypted_key

def main(param):
    params = ""
    loop = 1
    while (loop < len(param)):
        params = params+" "+param[loop]
        loop = loop + 1
    user_id = get_userid(param)
    host = get_host(param)
    if (len(user_id) > 0 and len(host) > 0):
        encrypted_password, decrypted_key = get_encrypted_password(host, user_id)
        if (len(encrypted_password) > 0 and len(decrypted_key) > 0):
            decrypted_password = decrypt(encrypted_password, decrypted_key)
            os.environ["PGPASSWORD"]=decrypted_password
            os.system("psql "+params)
        else:
            print("\nPasswords are not cached for "+user_id+" on "+host+", Operation Failed !!!")
            sys.exit(1)
    else:
        print("Username (or) Host is invalid !!!")
        sys.exit(1)
     
def help():
    print(usage)

if __name__ == '__main__':
    if check_os() == 0:
        if len(sys.argv) > 1:
            main(sys.argv)
        else:
            help()
    else:
        sys.exit(1)

