from Crypto.Cipher import AES
import base64

def encrypt(input, key):
    BLOCK_SIZE = 16
    PADDING = '{'
    pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
    encode_AES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
    cipher = AES.new(key, AES.MODE_ECB)
    encoded = encode_AES(cipher, input)
    return str(encoded)

def decrypt(input, key):
    PADDING = '{'
    decode_AES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
    cipher = AES.new(key, AES.MODE_ECB)
    decoded = decode_AES(cipher, input)
    return decoded

def keyes_key(input,n):
    input = input.rstrip("\n")
    output = ""
    while (n<64):
        output = output + input[n]
        n = n+4
    return output
