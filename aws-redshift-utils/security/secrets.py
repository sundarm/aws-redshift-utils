SECRETS={"aws_access_key_id":"",
        "aws_secret_access_key":"",
        "sql_server_password":""
		}

STATIC_CONFIG={"aws_default_region":"",
                "sql_server_host":"",
                "sql_server_database":"",
                "sql_server_driver":"{ODBC Driver 17 for SQL Server}",
                "sql_server_user":"",
                "sql_server_schema":""
				}
