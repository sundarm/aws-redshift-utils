"""
Developed by Sundar Manoharan
Usage: pgpassword [-h] | <subcmd> [<subcmd options>]
Options:
 -h or -? or --help   display this help

Subcommands:
 show        show the currently cached passwords (default)
 add         add a passowrd to cache
 delete      remove cached password(s)

This helps to cache the redshift password in the linux platform with the necessary encryption to the password.
"""

import sys
import os
import os.path
import getpass
import re
import random
import string
from Crypto.Cipher import AES
import base64

from utils.utils import check_file_exists
from utils.utils import generate_key
from utils.utils import check_os
from utils.utils import string_fill
from security.security import encrypt
from security.security import decrypt
from security.security import keyes_key

usage = """
Usage: pgpassword [-h] | <subcmd> [<subcmd options>]
Options:
 -h or -? or --help   display this help

Subcommands:
 show        show the currently cached passwords (default)
 add         add a passowrd to cache
 delete      remove cached password(s)
"""

password_file=".pgpassword"
user=os.getenv("USER")
home_dir=os.getenv("HOME")
password_file_path=home_dir+"/"+password_file

def add():
    if password_file_exists is False:
        create_password_file(password_file_path)
    
    if os.path.getsize(password_file_path) == 0:
	os.remove(password_file_path)
        create_password_file(password_file_path)
    try:
        host = raw_input("Enter Host : ")
        check_input(host)
        user_id = raw_input("Enter User ID : ")
        check_input(user_id)
        cached = check_cached(host, user_id)
        if cached:
            print("Password for "+user_id+" on "+host+" is already cached !, Please delete and add an entry with updated password...")
            sys.exit(122)
        password = getpass.getpass("Enter Password : ")
        check_input(password)
        file=open(password_file_path,"a+")
        lines = file.readlines()
        key_metadata=lines[0].split(":")
        ordinal_value = key_metadata[1].rstrip("\n")
        encrypted_key = key_metadata[2].rstrip("\n")
        keyees_key = key_metadata[3].rstrip("\n")
        actual_keyees_key = keyes_key(keyees_key, int(ordinal_value))
        decrypted_key = decrypt(encrypted_key, actual_keyees_key)
        encrypted_password = encrypt(password.rstrip("\n"),decrypted_key)
        file.write(host+":"+user_id+":"+encrypted_password+"\n")
        file.close()
        print("\nSuccessfully added user "+user_id+" to host "+host)
    except (KeyboardInterrupt):
        print("\nAction cancelled by User !")
        sys.exit(1)

def check_cached(host, user_id):
    file = open(password_file_path)
    lines = file.readlines()
    cached_flag = False
    if len(lines) <= 1:
        cached_flag = False
    else:
        line = 1 
        while (line < len(lines)):
            cached_password_entry = lines[line].split(":")
            cached_host = str(cached_password_entry[0].rstrip("\n"))
            cached_user = str(cached_password_entry[1].rstrip("\n"))
            if (host == cached_host and user_id == cached_user):
                cached_flag = True
                break
            line = line + 1
    file.close()
    return cached_flag
            

def delete():
    if password_file_exists is True:
        try:
            host = raw_input("Enter Host : ")
            check_input(host)
            user_id = raw_input("Enter User ID : ")
            check_input(user_id)
            file=open(password_file_path,"rw+")
            output = []
            for line in file:
                x=re.search("^"+host+":"+user_id+":*",line.strip())
                if x is False or x is None:
                    output.append(line)
            file.close()
            file = open(password_file_path,"w")
            file.writelines(output)
            file.close()
        except (KeyboardInterrupt):
            print("\nAction cancelled by User!")
            sys.exit(1)   
    else:
        print("\nNo password(s) are cached !")

def show():
    if password_file_exists:
       file = open(password_file_path)
       lines = file.readlines()
       if (len(lines) == 1 or len(lines) == 0):
	   print("\nNo Password(s) are cached !")
       else:
           print(string_fill("Host",64)+"\t"+string_fill("Username",15))
           print("---------------------------------------------------------------------------------------------------")
           for line in range(1,len(lines)):
               cached_password_entry = lines[line].split(":")
	       host = cached_password_entry[0].rstrip('\n')
               user = cached_password_entry[1].rstrip('\n')
               print(string_fill(host,64)+"\t"+string_fill(user,36))
    else:
       print("\nNo password(s) are cached !")
       sys.exit(0)

def help():
    print(usage)

def check_os():
    if (sys.platform == "linux" or sys.platform == "linux2"):
        return 0
    else:
        print("\nERROR: Please execute in Linux Platform...")
        return 1

def check_input(input):
    if len(input) < 1:
        print("\nNot a valid input, Operation cancelled by system !")
        sys.exit(256)

def create_password_file(password_file_path):
    f=open(password_file_path,"w+")
    ordinal_value = random.randint(0,3)
    actual_key = generate_key(16)
    keyees_key = generate_key(64)
    f.write("key:"+str(ordinal_value)+":"+encrypt(actual_key,keyes_key(keyees_key, ordinal_value))+":"+keyees_key+"\n")
    f.close()
    os.chmod(password_file_path,0600)

def check_option(option):
    if option in ['-h','-?','--help']:
        help()
    elif option == 'add':
        add()
    elif option == 'delete':
        delete()
    elif option == 'show':
        show()
    else:
        help()

if __name__ == '__main__':
    if check_os() == 0:
	password_file_exists=check_file_exists(password_file_path)
        if(len(sys.argv) == 2):
            check_option(sys.argv[1])
        elif (len(sys.argv) == 1 ):
            check_option("show")
        else:
            help()
    else:
        sys.exit(1)


