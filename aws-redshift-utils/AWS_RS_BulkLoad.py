#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import print_function
from collections import namedtuple

import argparse
import csv
import datetime
import json
import logging
import multiprocessing
import os
import subprocess
import sys
import time
from utils.utils import check_file_exists
from security.security import decrypt
from security.security import keyes_key

from utils.utils import check_os
from utils.utils import check_file_exists

from security.secrets import SECRETS
from security.secrets import STATIC_CONFIG

password_file=".pgpassword"
user=os.getenv("USER")
home_dir=os.getenv("HOME")
password_file_path=home_dir+"/"+password_file

AWS_ENV = os.environ

CRED = {
    "redshift": {
        "cluster": None,
        "user": None
    }
}

PARAM_COL_MAPPINGS = {
    "str": [
        "CP_DELIMITER", "CP_DATEFORMAT", "CP_TIMEFORMAT", "CP_NULL_AS",
        "CP_ENCODING", "CP_FIXEDWIDTH", "CP_ACCEPTINVCHARS"
    ],
    "int": [
        "CP_IGNOREHEADER", "CP_MAXERROR"
    ],
    "flag": [
        "CP_FILLRECORD", "CP_IGNOREBLANKLINES", "CP_REMOVEQUOTES",
        "CP_TRIMBLANKS", "CP_EMPTYASNULL", "CP_BLANKSASNULL", "CP_ESCAPE",
        "CP_ACCEPTANYDATE", "CP_TRUNCATECOLUMNS"
    ]
}

log = None
sqlserver = None


def main():
    """
    main is the entrypoint for this script. it loads the configuration, loads
    the credentials, then calls the correct 'cmd_...' function based on what
    subcommand was invoked.
    """

    try:
        config = setup_configuration()    
        # execute the specified subcommand
        if config["__cmd__"] == "copy":
            cmd_copy(config)
        elif config["__cmd__"] == "ccc":
            cmd_cleanup_copy_control(config)
    except Error as err:
        log.error(err)
        send_email()
        sys.exit(1)

def cmd_copy(config):
    """
    cmd_copy is called for the 'AWS_RS_BulkLoad copy' subcommand. it queries
    etl_ops.etl_redshift_infa_copy_params based on folder_name, workflow_name,
    session_name, and step_number. it checks that the query returned a result,
    then calls the main 'copy()' function.
    """

    query = """SELECT i.id as INFA_COPY_ID, p.*
        FROM etl_ops.etl_redshift_infa_copy_params i
        LEFT JOIN etl_ops.etl_redshift_copy_params p ON i.param_id = p.param_id
        WHERE folder_name=?
            AND workflow_name=?
            AND session_name=?
            AND step_number=?"""
    args = [(config["folder"]),(config["workflow"]),(config["session"]),(config["step_number"])]
    params = sqlserver.select(query, args)
    if len(params) == 0:
        raise Error("no parameterization found in database", query=query)
    copy(config, params[0])

def cmd_cleanup_copy_control(config):
    """marks old entries in etl_ops.etl_redshift_copy_control as 'timed out'"""

    log.info("timing out jobs older than", config["older_than"], "minute(s)")
    query = """update etl_ops.etl_redshift_copy_control
        set error_desc = 'zombie entry... timed out'
        where error_desc is null
            and completed_at is null
            and started_at < dateadd(minute,-?,current_timestamp)
        """
    args = [(config["older_than"])]
    sqlserver.execute(query,args)

def copy(config, params):
    """copy performs the core logic for copy jobs"""

    access_control_id = getattr(params, "ACCESS_CONTROL_ID", None)
    cluster = config["cluster"] or CRED["redshift"]["cluster"]
    username = config["user"] or CRED["redshift"]["user"]
    if not (access_control_id or (cluster and username)):
        raise Error("no access key given and cluster/user fallback missing.",
                    "please set the access_control_id in copy_params table")

    log.info("checking redshift access", cluster=cluster, user=username,
             access_control_id=access_control_id)
    access, password = get_redshift_access(cluster, username,
                                          access_control_id=access_control_id)
    log.info("got access", access)
    redshift = Redshift(host=access.HOST, port=access.PORT,
                            user=access.USERNAME, password=password)
    log.info("starting copy command", access_info=access)
    print_parameterization(params)
    job_id = None
    try:
        job_id = init_copy_job(config, params)
        print(job_id)
        log.info("created database entry for copy control", id=job_id)
        parameterization = gen_parameterization(params)
        log.info("parameterization: {}".format(parameterization))
        source_files = []
        if params.FILE_DIR.startswith("s3://"):
            log.info("data source has s3 prefix. no upload will be performed")
            # NOTE:
            #  commented out for now, can be enabled if needed in the future.
            #  main thing to consider is the interaction with
            #  'FILE_NAME_IS_PREFIX' flag
            # manifest["entries"].append({
            #     "url": os.path.join(params.FILE_DIR, params.FILE_NAME),
            #     "mandatory": True
            # })
        else:
            source_files = [params.FILE_NAME]
            if params.FILE_NAME_IS_PREFIX == 1:
                source_files = []
                for file_name in os.listdir(params.FILE_DIR):
                    if file_name.startswith(params.FILE_NAME):
                        source_files.append(file_name)

            file_name_filter = config.get("filename", None)
            if file_name_filter:
                if file_name_filter in source_files:
                    source_files = [file_name_filter]
                else:
                    source_files = []

        if len(source_files) == 0:
            raise Error("no source files found for copy job")

        manifest = {"entries": []}
        s3_root = os.path.join(params.S3_UPLOAD_BUCKET, params.S3_UPLOAD_DIR)
        s3_root = "s3://" + s3_root + "/"
        print(s3_root)
        #sys.exit(1)
        file_paths = []
        for file_name in source_files:
            source_file = os.path.join(params.FILE_DIR, file_name)
            transformed_file = apply_file_transformations(params, file_name)
            s3_dest = s3_root + file_name
            print(s3_dest)
            move = transformed_file != source_file
            upload_to_s3(transformed_file, s3_dest, move=move)
            file_paths.append({
                "source_file": source_file,
                "transformed_file": transformed_file,
                "s3_dest": s3_dest
            })
            manifest["entries"].append({"url": s3_dest, "mandatory": True})

        manifest_file = "copy.{}.manifest".format(job_id)
        manifest_file_path = os.path.join("/tmp", manifest_file)
        with open(manifest_file_path, "wb") as fh_manifest:
            json.dump(manifest, fh_manifest)

        s3_manifest = "s3://" + os.path.join(params.S3_UPLOAD_BUCKET,
                                             "tmp/manifests", manifest_file)
        log.info("uploading manifest file")
        upload_to_s3(manifest_file_path, s3_manifest, move=True)

        if params.TRUNC_TABLE_FLAG != 0:
            log.info("truncating target table", schema=params.TGT_DB_SCHEMA,
                     table=params.TGT_DB_TABLE)
            query = "TRUNCATE {0}.{1}".format(params.TGT_DB_SCHEMA,
                                              params.TGT_DB_TABLE)
            redshift.execute(params.TGT_DB_NAME, query)
            log.info("table truncate complete", schema=params.TGT_DB_SCHEMA,
                     table=params.TGT_DB_TABLE)

        query = """copy {}.{} from '{}' {}
                   statupdate off compupdate off manifest
                """.format(params.TGT_DB_SCHEMA, params.TGT_DB_TABLE,
                           s3_manifest, parameterization)
        log.info("executing copy command", job_id=job_id, query=query)

        # add the credentials after printing the query to prevent leaking
        # credentials in the log
        query += """ credentials
                    'aws_access_key_id={};aws_secret_access_key={}'
                 """.format(SECRETS["aws_access_key_id"],
                            SECRETS["aws_secret_access_key"])
        redshift.execute(params.TGT_DB_NAME, query)
        log.info("copy command completed", job_id=job_id)
        record_copy_job_result(job_id)

        if params.S3_ARCHIVE_DIR:
            s3_archive_root = "s3://" + os.path.join(params.S3_UPLOAD_BUCKET,
                                                     params.S3_ARCHIVE_DIR)
            timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f")
            for paths in file_paths:
                file_name = os.path.basename(paths["source_file"])
                archive_name = file_name + "_ETL_" + timestamp
                archive_dest = s3_archive_root + "/" + archive_name
                if paths["source_file"] == paths["transformed_file"]:
                    # the original, untransformed file is already in s3. we just
                    # need to move it to the archive location
                    upload_to_s3(paths["s3_dest"], archive_dest, move=True)
                else:
                    # a transformed version of the source file was uploaded so
                    # we need to upload the original to s3. we don't remove the
                    # local copy since that's handled by another flag. however,
                    # we can remove the transformed copy of the file that was
                    # uploaded.
                    upload_to_s3(paths["source_file"], archive_dest)
                    delete_from_s3(paths["s3_dest"])

        delete_from_s3(s3_manifest)
        if params.REMOVE_FILE_FLAG == 1:
            for paths in file_paths:
                log.info("removing local file", file=paths["source_file"])
                os.remove(paths["source_file"])

    except Error as err:
        if job_id:
            record_copy_job_result(job_id, err=err)
        send_email()
        log.error(err)
        sys.exit(1)


def upload_to_s3(local_file, dest, move=False):
    """
    upload_to_s3 moves or copies a local file to s3. if move=True, the local
    file will be removed on successful upload
    """

    cmdtype = "cp"
    if move:
        cmdtype = "mv"
    try:
        cmd = ["aws", "s3", cmdtype, local_file, dest]
        log.info("uploading file to s3", file=local_file, dst=dest)
        subprocess.call(cmd, env=AWS_ENV)
        log.info("s3 upload complete", file=local_file, dst=dest)
    except Exception as err:
        raise Error("aws cli error:", err, cmd=cmd)


def delete_from_s3(path, recursive=False):
    """
    delete_from_s3 removes the given path from s3 (recursively if requested)
    """

    log.info("deleting path from s3", path=path, recursive=recursive)
    cmd = ["aws", "s3", "rm", path]
    if recursive:
        cmd.append("--recursive")
    try:
        subprocess.call(cmd, env=AWS_ENV)
        log.info("deleted path from s3", path=path, recursive=recursive)
    except Exception as err:
        raise Error("aws cli error:", err, cmd=cmd)



def apply_file_transformations(params, filename):
    """
    apply_file_transformations performs various conversions on the input file.
    10/30/2017 added CFA application too for the below process
    these conversion can include:
    - removing newlines
    - prepending the source filename to each row of data
    - extracting data from excel files
    - converting to utf8
    """

    no_remove_newlines = []
    skip = any(app in params.FILE_DIR.lower() for app in no_remove_newlines)

    strip_newlines = not skip and params.REPLACE_NEWLINES_FLAG == 1
    # using getattr since this column was added in a migration
    prepend = None
    if getattr(params, "PREPEND_FILENAME_FLAG", None) == 1:
        prepend = filename

    file_path = os.path.join(params.FILE_DIR, filename)
    ext = os.path.splitext(filename)[1].lower()
    # using getattr since this column was added in a migration
    if ext in [".xls", ".xlsx"] and getattr(params, "EXCEL_SPEC", None):
        csv.register_dialect("excel_dialect", delimiter=params.CP_DELIMITER,
                             escapechar="\\", doublequote=False,
                             quoting=csv.QUOTE_ALL, quotechar="\"")
        output = "{}_{}.extracted.csv".format(file_path, params.PARAM_ID)
        Excel2CSV(file_path, output, params.EXCEL_SPEC, "excel_dialect",
                  strip_newlines, prepend=prepend)
        file_path = output
    else:
        if skip:
            log.info("skipping character replacement", file=file_path,
                     skip_list=no_remove_newlines)

        if strip_newlines:
            output = file_path + ".nonewlines"
            replace_characters(file_path, output, params.CP_DELIMITER,
                               prepend=prepend)
            file_path = output
        elif prepend:
            output = file_path + ".prepended"
            with open(file_path, "rb") as fh_in, open(output, "wb") as fh_out:
                for line in fh_in:
                    if len(line.strip()) > 0:
                        line = filename + params.CP_DELIMITER + line
                    fh_out.write(line)
            file_path = output

    og_file = os.path.join(params.FILE_DIR, filename)
    utf8_file = file_path
    # using getattr since this column was added in a migration
    src_encoding = getattr(params, "FILE_ENCODING", None)
    conv_fn = getattr(params, "UTF8_CONVERSION_FUNC", None)
    if src_encoding:
        utf8_file = convert2utf8(file_path, src_encoding)
    elif conv_fn == 0:
        utf8_file = convert2utf8_v0(file_path)
    elif conv_fn == 1:
        utf8_file = convert2utf8_v1(file_path)

    if file_path != og_file and file_path != utf8_file:
        # file was previously transformed... remove it
        os.remove(file_path)
    file_path = utf8_file
    return file_path

def init_copy_job(config, params):
    """
    init_copy_job waits until the number of running copy jobs is less than
    "max_active_jobs" and then creates an entry in
    etl_ops.etl_redshift_copy_control. this entry acts as a marker that the job
    is running. params is an instance of the DB) class
    """

    queued_at = datetime.datetime.now()
    """ queueing system is deprecated at the moment
    while True:
        query = ""select count(*) as active
                   from etl_ops.etl_redshift_copy_control
                   where completed_at is null and error_desc is null""
        results = sqlserver.select(query)
        active = results[0].ACTIVE
        log.info("active jobs: {}".format(active))
        if active < config["max_active_jobs"]:
            break
        log.info("too many active jobs. will retry in",
                 config["retry_seconds"], "seconds")
        send_email()
        time.sleep(config["retry_seconds"])
    """

    query = """select coalesce(max(id),0)+1 as NEXT from etl_ops.etl_redshift_copy_control where 1 = ?"""
    args = [(1)]
    print(params)
    results = sqlserver.select(query, args)
    print("results is ", results)
    job_id = results[0].NEXT
    print("job id is ", job_id)

    query = """insert into etl_ops.etl_redshift_copy_control
               (id,param_id,queued_at,infa_copy_id,ext_load_id) values (?,?,?,?,?)"""
    # we use getattr() for "INFA_COPY_ID" and "EXT_LOAD_ID" since
    # they may not be set. if they aren't set we default to None (null)
    args = [(job_id),(params.PARAM_ID),(queued_at),
              (getattr(params, "INFA_COPY_ID", None)),
              (getattr(params, "EXT_LOAD_ID", None))
    ]
    sqlserver.insert(query, args)
    return job_id


def record_copy_job_result(jid, err=None):
    """
    record_copy_job_result marks the job with `id` as completed and with the
    (possibly null) error description
    """

    if err:
        err = str(err)
    log.info("updating control table", job_id=jid, err=err)
    query = """UPDATE etl_ops.etl_redshift_copy_control
        SET completed_at = CURRENT_TIMESTAMP, error_desc = ?
        WHERE id = ?"""
    args = [(err),(jid)]
    sqlserver.execute(query, args)
    log.info("control table updated", job_id=jid, err=err)


def print_parameterization(params):
    """
    print_parameterization is a helper function that prints a record from the
    etl_ops.etl_redshift_copy_params table in a human readable format. params
    are a named tuple
    """

    extra = [
        "PARAM_ID", "FILE_DIR", "FILE_NAME", "FILE_NAME_IS_PREFIX",
        "TGT_DB_NAME", "TGT_DB_SCHEMA", "TGT_DB_TABLE",
        "S3_UPLOAD_BUCKET", "S3_UPLOAD_DIR", "S3_ARCHIVE_DIR",
        "REMOVE_FILE_FLAG", "TRUNC_TABLE_FLAG", "REPLACE_NEWLINES_FLAG",
        "EXCEL_SPEC", "ACCESS_CONTROL_ID", "UTF8_CONVERSION_FUNC",
        "PREPEND_FILENAME_FLAG", "FILE_ENCODING"
    ]

    all_column_names = sum(PARAM_COL_MAPPINGS.values(), extra)
    max_col_len = max([len(x) for x in all_column_names])
    fmt = "{:<"+str(max_col_len+2)+"}: {}"
    for k in extra:
        print(fmt.format(k, repr(getattr(params, k, None))))
    fmt = "  {:<"+str(max_col_len)+"}: {}"
    for param_kind in PARAM_COL_MAPPINGS:
        param_names = PARAM_COL_MAPPINGS[param_kind]
        print("[{} params]".format(param_kind))
        for param_name in param_names:
            param = repr(getattr(params, param_name, None))
            print(fmt.format(param_name[3:].replace("_", " "), param))
    print("")


def gen_parameterization(params):
    """
    gen_parameterization return a string that can be added to the
    redshift COPY command as is. it is constructed from columns in
    etl_ops.etl_redshift_copy_params that begin with 'CP_'. it uses the global
    map `param_col_mappings` to determine whether the parameter is a string,
    integer, or flag type.

    the parameter is not include if:
        string type and value is null or empty
        integer type and value is 0
        flag type and value is 0

    note that sqlserver does not differentiate between and empty string and null
    """

    parameterization = ""
    for name in params._fields:
        value = getattr(params, name)
        name = name.upper()
        if not name.startswith("CP_"):
            continue
        # remove the 'CP_' prefix and convert '_' to ' ' to match the actual
        # redshift parameter name
        param_name = name[3:].replace("_", " ")
        if param_name == "DELIMITER":
            # aws redshift expects the delimiter in escaped octal form but
            # python returns the delimiter from the database in hex. we convert
            # the hex to octal here and make sure it is 3 characters long
            value = "\\" + oct(ord(value)).zfill(3)[-3:]
        if name in PARAM_COL_MAPPINGS["str"] and value and len(value) > 0:
            parameterization += " {} '{}' ".format(param_name, value)
        elif name in PARAM_COL_MAPPINGS["int"] and value != 0:
            parameterization += " {} {} ".format(param_name, value)
        elif name in PARAM_COL_MAPPINGS["flag"] and value != 0:
            parameterization += " {} ".format(param_name)
    return parameterization


def convert2utf8_v0(filename):
    """
    convert2utf8_v0 attempts to convert a file to utf8 format using vim.

    this is a deprecated method (prefer convert2utf8_v1)
    """

    timestamp = datetime.datetime.now().strftime("%s")
    vimrc_file = "/tmp/{}.{}.vimrc".format(timestamp, os.getpid())
    with open(vimrc_file, "wb") as fh_vimrc:
        fh_vimrc.write("set shortmess+=A\nset nobackup\nset noswapfile\n")
    log.info("converting file to utf8 (v0)", file=filename)
    proc = subprocess.Popen(["ex", "-u", vimrc_file, "-V", filename],
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    utf8_file = filename.replace(" ", "") + ".utf8"
    cmd_input = "set nobomb\nset fenc=utf-8\nwq! " + utf8_file + "\n"
    stdout, stderr = proc.communicate(input=cmd_input)
    os.remove(vimrc_file)
    if proc.returncode != 0:
        raise Error("failed to convert file to utf-8",
                    returncode=proc.returncode, err=stderr)
    log.info("converted file to utf8 (v0)", file=filename, stdout=stdout)
    return utf8_file


def convert2utf8_v1(filename):
    """
    convert2utf8_v1 attempts to convert a file to utf8 format using python
    """

    import codecs

    log.info("converting file to utf8 (v1)", file=filename)

    is_utf8 = True
    with open(filename, "rb") as fh_in:
        for line in fh_in:
            try:
                line.decode("utf8")
            except UnicodeDecodeError:
                is_utf8 = False
                break

    formats = ["windows-1252", "iso-8859-1"]
    if is_utf8:
        log.info("file is readable as utf8. no conversion needed",
                 filename=filename)
        return filename
    else:
        log.info("file is not readable as utf8. trying common formats",
                 formats=formats)

    utf8_file = filename + ".utf8"
    for enc in formats:
        try:
            log.info("trying to read file as", enc, filename=filename)
            with codecs.open(filename, "rb", enc) as fh_in:
                with codecs.open(utf8_file, "wb", "utf8") as fh_out:
                    for line in fh_in:
                        fh_out.write(line)
            log.info("converted file from", enc, "to utf8", filename=filename)
            return utf8_file
        except UnicodeDecodeError:
            log.info("file not readable as", enc, filename=filename)

    raise Error("unable to convert file to utf8",
                filename=filename, attempted_encodings=formats)


def convert2utf8(filename, src_encoding):
    """
    convert2utf8
    """

    import codecs

    utf8_file = filename + ".utf8"
    with codecs.open(filename, "rb", src_encoding) as fh_in:
        with codecs.open(utf8_file, "wb", "utf8") as fh_out:
            for line in fh_in:
                fh_out.write(line)
    return utf8_file

def get_encrypted_password(host, user_id):
    encrypted_password=""
    decrypted_key = ""
    password_file_exists = check_file_exists(password_file_path)
    if password_file_exists is True:
        file = open(password_file_path)
        lines = file.readlines()
        key_metadata = lines[0].split(":")
        ordinal_value = key_metadata[1].rstrip("\n")
        encrypted_key = key_metadata[2].rstrip("\n")
        keyees_key = key_metadata[3].rstrip("\n")
        actual_keyees_key = keyes_key(keyees_key, int(ordinal_value))
        decrypted_key = decrypt(encrypted_key, actual_keyees_key)
        for line in range(1,len(lines)):
            cached_password_entry = lines[line].split(":")
            cached_host = str(cached_password_entry[0].rstrip("\n"))
            cached_user = str(cached_password_entry[1].rstrip("\n"))
            cached_encrypted_password = str(cached_password_entry[2].rstrip("\n"))
            if (host == cached_host and user_id == cached_user):
                encrypted_password = cached_encrypted_password
                break
        file.close()
    else:
        encrypted_password = ""
    return encrypted_password, decrypted_key


def get_redshift_access(cluster, username, access_control_id=None):
    """
    retrieve the redshift password for a given cluster/user. if
    access_control_id is set, it will be preferred. otherwise, the
    cluster/username string are used
    """
    if access_control_id:
        query = """select * from etl_ops.etl_redshift_access_control
            where id=?"""
        args = [(access_control_id)]
        results = sqlserver.select(query, args)
    else:
        query = """select * from etl_ops.etl_redshift_access_control
            where cluster_name=? and username=?"""
        args = [(cluster), (username)]
        results = sqlserver.select(query, args)
    if len(results) != 1:
        raise Error("access control query did not return exactly 1",
                    query=query, results=results)
    access = results[0]
    log.info(str(access[2]))
    log.info(str(access[4]))
    encrypted_password, decrypted_key = get_encrypted_password(str(access[2]), str(access[4]))
    decrypted_password = decrypt(encrypted_password, decrypted_key)
    return access, decrypted_password


def setup_configuration():
    """setup_configuration"""

    global sqlserver

    AWS_ENV["AWS_ACCESS_KEY_ID"] = SECRETS["aws_access_key_id"]
    AWS_ENV["AWS_SECRET_ACCESS_KEY"] = SECRETS["aws_secret_access_key"]
    AWS_ENV["AWS_DEFAULT_REGION"] = STATIC_CONFIG["aws_default_region"]

    # parse the command line arguments
    config = parse_args()

    credentials_file_path = config.get("credentials_file", None)
    print(credentials_file_path)
    if credentials_file_path and os.path.isfile(credentials_file_path):
        with open(credentials_file_path) as fh_credentials:
            data = json.load(fh_credentials)
            if "redshift" in data:
                drs = data["redshift"]
                if "cluster" in drs:
                    CRED["redshift"]["cluster"] = drs["cluster"]
                if "user" in drs:
                    CRED["redshift"]["user"] = drs["user"]

    print(CRED["redshift"]["user"])

    sqlserver = SQLServer(host=STATIC_CONFIG["sql_server_host"],
                    database=STATIC_CONFIG["sql_server_database"],
                    user=STATIC_CONFIG["sql_server_user"],
                    password=SECRETS["sql_server_password"],
                    driver=STATIC_CONFIG["sql_server_driver"])
    return config


class Redshift(object):
    """
    Redshift is used to query redshift
    """

    def __init__(self, host=None, port=None, user=None, password=None):
        self.host = host
        self.port = port
        self.user = user
        self.password = password

    def execute(self, database, query, **kwargs):
        """
        executes the given query against the given database. the returned
        result is always None
        """

        return self._execute(database, query, False, **kwargs)

    def select(self, database, query, **kwargs):
        """
        executes the given query against the given database. all results from
        are returned as DBO objects
        """

        return self._execute(database, query, True, **kwargs)

    def _execute(self, database, query, is_select, **kwargs):
        """perform the actual redshift query"""
        try:
            con = psycopg2.connect(dbname=database, host=self.host,
                                   port=self.port, user=self.user,
                                   password=self.password)
            cur = con.cursor()
            cur.execute(query, kwargs)
            results = []
            if is_select:
                columns = [i[0].upper() for i in cur.description]
                results = [namedtuple('DBO', columns)(*row) for row in cur]
            con.commit()
            cur.close()
            con.close()
            return results
        except Exception as err:
            raise Error("database query failed:", err)


class SQLServer(object):
    """
    SQLServer is used to query redshift
    """

    def __init__(self, host=None, database=None, user=None, password=None, driver=None):
        self.host = host
        self.database = database
        self.user = user
        self.password = password
        self.driver = driver

    def select(self, query, args):
        """executes the given query and returns all results as DBO objects"""

        return self._execute(query, True, args)

    def execute(self, query, args):
        """executes the given query. the returned result is always None"""

        return self._execute(query, False, args)

    def insert(self, query, args):
        """
        performs an insert. the query given must be a format string containing
        'placeholders' and 'columns'
        """
        placeholders = []
        for arg in args:
            val = (arg)
            placeholders.append(val)
        print(query)
        print(placeholders)
        return self._execute(query, False, placeholders)

    def _execute(self, query, is_select, args):
        """perform the actual SQLServer query"""
        log.info("executing SQLServer sql", query=query, params=args)
        try:
            con = pyodbc.connect(DRIVER=self.driver, SERVER=self.host, DATABASE=self.database, UID = self.user, PWD = self.password)
            cur = con.cursor()
            print(args)
            cur.execute(query, args)
            results = []
            log.info("query execution complete")
            if is_select:
                log.info("extracting query results")
                columns = [i[0].upper() for i in cur.description]
                print(columns)
                results = [namedtuple('DBO', columns)(*row) for row in cur.fetchall()]
            cur.close()
            con.commit()
            con.close()
            return results
        except Exception as err:
            raise Error("database query failed:", err, query=query)


class Excel2CSV(object):
    """
    Excel2CSV extracts data from an excel file (xls or xlsx format)
    """

    def __init__(self, input_file, output, spec, dialect, strip_newlines, prepend=None):
        self.spec = {}
        spec = spec.strip()
        while len(spec) > 0:
            spec = self.parse_sheet_spec(spec).strip()

        ext = os.path.splitext(input_file)[1].lower()
        if ext == ".xlsx":
            data = self.extract_xlsx_data(input_file)
        elif ext == ".xls":
            data = self.extract_xls_data(input_file)
        else:
            raise Error("file does not have proper extension (xls, xlsx)",
                        file=input_file)

        data = self.prune_columns(data)
        with open(output, "wb") as fh_out:
            writer = csv.writer(fh_out, dialect=dialect)
            for row in data:
                if strip_newlines:
                    row = [replace_newlines(r) for r in row]
                if prepend:
                    row = [prepend] + row
                writer.writerow(row)

    def extract_xlsx_data(self, filename):
        """extract_xlsx_data"""

        import openpyxl
        import xlrd

        def enc_cell(cidx, cell, sheet_spec):
            """
            enc_cell converts an openpyxl cell to a string
            """

            if not cell or cell.value is None or cell.value == "":
                return ""
            cell_type = None
            for col_def in sheet_spec["col_defs"]:
                if cidx in col_def["columns"]:
                    cell_type = col_def["type"]
                    break

            if cell_type == "date" and isinstance(cell.value, (long, float, int)):
                date = xlrd.xldate_as_tuple(cell.value, 0)
                return "{:02d}/{:02d}/{:02d} {:02d}:{:02d}:{:02d}".format(date[1],
                                                                          date[2],
                                                                          date[0],
                                                                          date[3],
                                                                          date[4],
                                                                          date[5])

            if isinstance(cell.value, datetime.datetime):
                return cell.value.strftime("%m/%d/%Y %H:%M:%S")
            if isinstance(cell.value, bool):
                return "TRUE" if cell.value else "FALSE"
            if isinstance(cell.value, unicode):
                return cell.value.encode("utf8")
            return str(cell.value)

        data = []
        workbook = openpyxl.load_workbook(filename=filename, data_only=True)
        for i, worksheet in enumerate(workbook.worksheets):
            sheet_spec = self.check_spec(i, worksheet.title)
            if not sheet_spec:
                continue

            hidden_rows = set()
            for k in worksheet.row_dimensions:
                row_info = worksheet.row_dimensions[k]
                if row_info.hidden:
                    hidden_rows.add(int(k)-1)
            hidden_cols = set()
            for k in worksheet.column_dimensions:
                col_info = worksheet.column_dimensions[k]
                if col_info.hidden:
                    hidden_cols.update(range(col_info.min-1, col_info.max))

            rows = []
            for j, row in enumerate(worksheet):
                if j < sheet_spec["data_start"]:
                    rows.append([]) # placeholder row
                    continue
                row = [enc_cell(j, c, sheet_spec) for j, c in enumerate(row)]
                rows.append(row)

            cell_ranges = []
            for cell_range in worksheet.merged_cell_ranges:
                cell_ranges.append(self.parse_range(cell_range))
            data += self.filter_sheet_data(rows, sheet_spec, hidden_rows,
                                           hidden_cols, cell_ranges)
        return data

    def col2num(self, col):
        """col2num"""

        num = 0
        for letter in col:
            num = num * 26 + (ord(letter.upper()) - ord('A')) + 1
        return num

    def parse_range(self, range_string):
        """parse_range"""

        cell1, cell2 = range_string.split(":", 1)
        c1letter = cell1.rstrip("0123456789")
        c2letter = cell2.rstrip("0123456789")
        cell1row = int(cell1[len(c1letter):]) - 1
        cell2row = int(cell2[len(c2letter):]) - 1
        cell1col = self.col2num(c1letter) - 1
        cell2col = self.col2num(c2letter) - 1
        return cell1row, cell2row, cell1col, cell2col

    def extract_xls_data(self, filename):
        """extract_xls_data"""

        import xlrd

        def enc_cell(cidx, cell, sheet_spec, datemode):
            """
            enc_cell converts an xlrd cell to a string
            """

            if not cell or cell.value is None or cell.value == "":
                return ""
            if cell.ctype in [xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK,
                              xlrd.XL_CELL_ERROR]:
                return ""

            cell_type = None
            for col_def in sheet_spec["col_defs"]:
                if cidx in col_def["columns"]:
                    cell_type = col_def["type"]
                    break

            if (cell_type == "date" or cell.ctype == xlrd.XL_CELL_DATE) and isinstance(cell.value, (long, float, int)):
                date = xlrd.xldate_as_tuple(cell.value, datemode)
                return "{:02d}/{:02d}/{:02d} {:02d}:{:02d}:{:02d}".format(date[1],
                                                                          date[2],
                                                                          date[0],
                                                                          date[3],
                                                                          date[4],
                                                                          date[5])

            if cell.ctype == xlrd.XL_CELL_TEXT:
                return cell.value.encode("utf8")
            if cell.ctype == xlrd.XL_CELL_NUMBER:
                return str(cell.value)
            if cell.ctype == xlrd.XL_CELL_BOOLEAN:
                return "TRUE" if str(cell.value) == "1" else "FALSE"
            if isinstance(cell.value, datetime.datetime):
                return cell.value.strftime("%m/%d/%Y %H:%M:%S")
            return str(cell.value)

        data = []
        workbook = xlrd.open_workbook(filename, formatting_info=True)
        for i, worksheet in enumerate(workbook.sheets()):
            sheet_spec = self.check_spec(i, worksheet.name)
            if not sheet_spec:
                continue
            hidden_rows = set()
            for i in worksheet.rowinfo_map:
                row_info = worksheet.rowinfo_map[i]
                if row_info.hidden == 1:
                    hidden_rows.add(int(i))
            hidden_cols = set()
            for i in worksheet.colinfo_map:
                col_info = worksheet.colinfo_map[i]
                if col_info.hidden == 1:
                    hidden_cols.add(int(i))

            rows = []
            for j in range(worksheet.nrows):
                if j < sheet_spec["data_start"]:
                    rows.append([]) # placeholder row
                    continue
                row = worksheet.row(j)
                row = [enc_cell(k, c, sheet_spec, workbook.datemode) for k, c in enumerate(row)]
                rows.append(row)

            cell_ranges = []
            data += self.filter_sheet_data(rows, sheet_spec, hidden_rows,
                                           hidden_cols, cell_ranges)
        return data

    def parse_sheet_spec(self, spec):
        """parse_sheet_spec"""

        end = spec.find(":")
        if end == -1:
            raise Error("invalid spec:", spec)

        sheet = spec[:end].strip()
        col_defs = []

        spec = spec[end+1:]
        idx1 = spec.find(",")
        idx2 = spec.find("(")
        if idx1 == idx2 == -1:
            data_start = spec.strip()
            spec = ""
        elif idx1 >= 0 and (idx2 == -1 or idx1 < idx2):
            # no column defs
            data_start = spec[:idx1].strip()
            spec = spec[idx1+1:]
        else:
            data_start = spec[:idx2].strip()
            spec = spec[idx2+1:]
            end = spec.find(")")
            if end == -1:
                raise Error("invalid spec:", spec)

            defs = spec[:end].split(",")
            for col_def in defs:
                col_def = col_def.strip()
                if len(col_def) == 0:
                    continue

                req, skip, unmerge = False, False, False
                while len(col_def) > 0 and col_def[0] in ["*", "!", "+"]:
                    if col_def[0] == "*":
                        req = True
                    if col_def[0] == "!":
                        skip = True
                    if col_def[0] == "+":
                        unmerge = True
                    col_def = col_def[1:]

                if len(col_def) == 0:
                    continue

                idx_and_type = col_def.split(":", 1)
                col_idx_lo = idx_and_type[0].strip()
                col_idx_hi = col_idx_lo
                if "-" in col_idx_lo:
                    # merged columns
                    col_range = col_idx_lo.split("-", 1)
                    col_idx_lo = col_range[0].strip()
                    col_idx_hi = col_range[1].strip()
                col_type = None
                if len(idx_and_type) > 1:
                    col_type = idx_and_type[1].strip().lower()
                    if col_type not in ["date"]:
                        raise Error("invalid column type:", col_type)
                try:
                    col_range = set(range(int(col_idx_lo)-1, int(col_idx_hi)))
                    col_defs.append({
                        "unmerge": unmerge,
                        "skip": skip,
                        "required": req,
                        "columns": col_range,
                        "type": col_type
                    })
                except:
                    raise Error("invalid column index:", col_idx_lo, col_idx_hi)
            spec = spec[end+1:]

        try:
            data_start = int(data_start) - 1
        except:
            raise Error("invalid data start:", data_start)

        try:
            # if we can parse 'sheet' as an integer we treat it as a sheet
            # number.  we subtract 1 to get the array index (instead of sheet
            # number)
            sheet = str(int(sheet) - 1)
        except:
            pass

        # make sure column defs do not overlap
        all_cols = set()
        for col_def in col_defs:
            if len(col_def["columns"].intersection(all_cols)) > 0:
                raise Error("column definitions overlap")
            all_cols.update(col_def["columns"])

        self.spec[sheet] = {"data_start": data_start, "col_defs": col_defs}
        return spec

    def check_spec(self, idx, spec_name):
        """check_spec"""

        if str(idx) in self.spec:
            return self.spec[str(idx)]
        elif spec_name in self.spec:
            return self.spec[spec_name]
        elif "*" in self.spec:
            return self.spec["*"]
        return None

    def filter_sheet_data(self, raw_rows, sheet_spec, hidden_rows, hidden_cols, cell_ranges):
        """filter_sheet_data"""

        rows = []
        carry_over = {}
        required_cols = set()
        for col_def in sheet_spec["col_defs"]:
            if col_def["skip"]:
                hidden_cols.update(col_def["columns"])
                continue
            col_idx_lo = min(col_def["columns"])
            if len(col_def["columns"]) > 1:
                # check column defs for merged columns. if there are merged
                # columns set data value should be in the left most cell so we
                # can essentially ignore all the columns except for the first
                hidden_cols.update(col_def["columns"].difference([col_idx_lo]))
            if col_def["unmerge"]:
                carry_over[col_idx_lo] = ""
            if col_def["required"]:
                required_cols.add(col_idx_lo)

        # a simplifying assumption
        # when requesting "unmerge" it is either a single column 'u3'
        # or a part of merged columns 'u3-5'
        # if single -> that column is not a part of merged cells (headers?)
        # multi     -> min col is leftmost of merge cells
        # for each merged cell range that start in column and >= data_start:
        # get value of first, carry over to subsequent

        for i, row in enumerate(raw_rows):
            if i < sheet_spec["data_start"] or i in hidden_rows:
                continue

            # all cell_ranges for this row
            row_ranges = [cr for cr in cell_ranges if i >= cr[0] and i <= cr[1]]
            if len(row_ranges) > 0:
                for cidx in carry_over:
                    # check if there's a cell range for this specific column
                    col_ranges = [cr for cr in row_ranges if cidx == cr[2]]
                    if len(col_ranges) > 1:
                        raise Error("wtf", i, col_ranges)
                    if len(col_ranges) == 1:
                        col_range = col_ranges[0]
                        if col_range[0] == i:
                            # this row is the first, remember it for later
                            carry_over[cidx] = row[cidx]
                        else:
                            row[cidx] = carry_over[cidx]

            if any(row[cidx] == "" for cidx in required_cols):
                continue
            if all([c == "" for c in row]):
                continue
            rows.append([c for j, c in enumerate(row) if j not in hidden_cols])

        return rows

    def prune_columns(self, rows):
        """prune_columns"""

        if len(rows) == 0:
            return []

        col_lengths = set()
        for row in rows:
            col_lengths.add(len(row))

        max_col = max(col_lengths)
        if len(col_lengths) > 1:
            rows = [r+[""]*(max_col-len(r)) for r in rows]
        end = max_col
        while all(c == "" for c in [r[end-1] for r in rows]):
            end -= 1
        if end != max_col:
            rows = [r[:end] for r in rows]
        return rows


class Error(Exception):
    """
    error is a class for application level exceptions. it has a specialized
    constructor to simplify creating an exception with contextual data all none
    keyword args are concatenated using spaces. if there are any keyword args,
    they are appended in "key=value" form and joined with a comma.

    for example:
      details = "error details"
      print Error("an error occured:", details, foo=1, bar=False)
    will print:
      an error occured: error details (foo=1, bar=False)
    """

    def __init__(self, *args, **kwargs):
        msg = " ".join([str(a) for a in args])
        if len(kwargs) > 0:
            kvs = ["{}={}".format(k, kwargs[k]) for k in kwargs]
            msg += " ({})".format(", ".join(kvs))
        super(Error, self).__init__(msg)


class Log(object):
    """
    Log is used to print messages to stdout
    """

    def __init__(self, log_file):
        self.log_file = log_file
        self.lock = multiprocessing.Lock()

    def info(self, *args, **kwargs):
        """prepends INFO, does not send an email"""
        self._log("INFO", False, *args, **kwargs)

    def warn(self, *args, **kwargs):
        """prepends WARNING, sends an email"""
        self._log("WARNING", True, *args, **kwargs)

    def issue(self, *args, **kwargs):
        """prepends ISSUE, sends an email"""
        self._log("ISSUE", True, *args, **kwargs)

    def error(self, *args, **kwargs):
        """prepends ERROR, sends an email"""
        self._log("ERROR", True, *args, **kwargs)

    def failed(self, *args, **kwargs):
        """prepends FAILED, sends an email"""
        self._log("FAILED", True, *args, **kwargs)

    def no_email(self, level, *args, **kwargs):
        """prepends 'level', does not send an email"""
        self._log(level, False, *args, **kwargs)

    def _log(self, level, email, *args, **kwargs):
        """format and print the log message"""
        msg = " ".join([str(a) for a in args])
        timestamp = datetime.datetime.now().strftime("%Y%m%d:%H:%M:%S:%f")
        msg_log = "{} {}: {}".format(timestamp, level, msg)
        if len(kwargs) > 0:
            kwstrs = ["{}={}".format(k, kwargs[k]) for k in kwargs]
            msg_log += " ({})".format(", ".join(kwstrs))
        print(msg_log)
        if self.log_file:
            with open(self.log_file, "a") as fh_log:
                fh_log.write(msg_log+"\n")
        if level in ["WARNING", "ISSUE", "ERROR", "FAILED"]:
            send_email()

def send_email():
    log.info("Email is Unimplemented")



def parse_args():
    """
    parse_args extract / validates the command line arguments and returns them
    as a regular python dictionary.
    """

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="__cmd__")

    copy_parser = subparsers.add_parser("copy", help="""
        perform a copy by specifying an informatica folder/workflow/session
        (and optional step number)""")
    copy_parser.add_argument("-f", "--folder", dest="folder", required=True,
                             type=str, help="""
        informatica folder name (case sensitive)""")
    copy_parser.add_argument("-w", "--workflow", dest="workflow",
                             required=True, type=str, help="""
        informatica workflow name (case sensitive)""")
    copy_parser.add_argument("-s", "--session", dest="session", required=True,
                             type=str, help="""
        informatica session name (case sensitive)""")
    copy_parser.add_argument("-step", "--step-number", dest="step_number",
                             default=1, type=int, help="""
        step number within the informatica session""")

    ccc_parser = subparsers.add_parser("ccc", help="""
        cleanup copy jobs. reap zombie jobs in redshift_copy_control table""")
    ccc_parser.add_argument("older_than", nargs="?", type=int, default=180,
                            help="""
        jobs started earlier than this (in minutes) will be marked as
        'timed out'. default = 180""")

    # these arguments are common to all subcommands that perform an actual copy
    # job
    for sub_parser in [copy_parser]:
        sub_parser.add_argument("-filename", type=str, dest="filename",
                                default=None, help="""
            only use file exactly matching this string as the source
            filename""")
        sub_parser.add_argument("-user", "--username", type=str, dest="user",
                                default=None, help="""
            dev override. user to connect to redshift as. must match a value in
            the etl_ops.etl_redshift_access_control table""")
        sub_parser.add_argument("-cluster", "--cluster-name", type=str,
                                dest="cluster", default=None, help="""
            dev override. redshift cluster to connect to. must match a value in
            the etl_ops.etl_redshift_access_control table""")
        sub_parser.add_argument("--max-active-jobs", dest="max_active_jobs",
                                type=int, default=1000000, help="""
            if the number of active copy jobs is greater than or equal to this
            number, the current copy job will wait. (default = 1000000)""")
        sub_parser.add_argument("--retry-seconds", dest="retry_seconds",
                                type=int, default=10, help="""
            number of seconds to wait if 'max-active-jobs' are already active.
            (default = 10)""")

    # the '-cred' argument is deprecated. however, there are references to this
    # in informatica workflows that is used to indicate which cluster to
    # connect to. (credentials.json = qa2, credentials1.json = qa3)
    for sub_parser in [copy_parser]:
        sub_parser.add_argument("-cred", "--credentials-file", type=str,
                                dest="credentials_file", default="credentials.json",
                                help="deprecated")

    args = vars(parser.parse_args())

    # informatica likes to add extra newline characters to its command line
    # arguments. to get around this we strip leading/trailing whitespace on any
    # string argument
    for k in args:
        value = args[k]
        if isinstance(value, str):
            args[k] = value.strip()
    return args

if __name__ == "__main__":
    log = Log(None)
    try:
        import pyodbc
        import psycopg2

        main()

    except Exception as excp:
        import traceback
        log.failed("Unhandled exception : {0}\n\n{1}".format(excp, traceback.format_exc()))
        send_email()
        sys.exit(1)
