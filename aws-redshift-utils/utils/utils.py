import os
import string
import random
import sys

def check_file_exists(password_file):
    exists_flag=os.path.exists(password_file)
    return exists_flag

def generate_key(n):
    key = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(n)])
    return str(key)

def check_os():
    if (sys.platform == "linux" or sys.platform == "linux2"):
        return 0
    else:
        print("\nERROR: Please execute in Linux Platform...")
        return 1

def string_fill(input, n):
    while len(input) < n:
        input = input + " "
    return input
