# AWS Redshift Bulk Load & pgsql Utility

## Installation Setup 

Download the project as a zip (or) tar file format, whichever works for you and un zip/tar in your local file system.

For AWS Redshift Bulk load utility to work, please setup the secrets.py in **aws-redshift-utils/security/secrets.py**. Provide the necessary values for the below variables,

```
SECRETS={"aws_access_key_id":"",
        "aws_secret_access_key":"",
        "sql_server_password":""
		}

STATIC_CONFIG={"aws_default_region":"",
                "sql_server_host":"",
                "sql_server_database":"",
                "sql_server_driver":"{ODBC Driver 17 for SQL Server}",
                "sql_server_user":"",
                "sql_server_schema":""
				}
```

Once setting up the secrets, please execute ```pip install -r requirements.txt``` to initialize the required pip packages for the utility.

Now run ```python deploy.py``` command to compile the python code to get the **.pyc** files for the project folder. Move the deploy output tar file to the necessary location for the script execution.

It is always recommended to use the compiled versions of **.py** files, also not to move the compiled code between machines which has the different releases of python. Execution will fail due to the different in the magic number for the compiled python file.


## pgsql & pgpassword Utility

pgpassword utility helps to securely store the user password using AES encryption algorithm on user home directory ~/.pgpassword
pgsql utility uses the securely stored password in ~/.pgpassword and executes the psql command. Based on the parameters passed to the pgsql python utility without manually passing the Redshift DB user password explicitly.

## Caching the user password
```python pgpassword.py add```

Add command helps to add the host and user details of the user to cache the password locally. This will prompt host , user and password for the password to be cached. This process adds an new file ~/.pgpassword with the encrypted password entry for the above mentioned credential.

```python pgpassword.py delete```

Delete command helps to remove the cached password of a host and user from the ~/.pgpassword file. There is no update command in this process, which requires deletion and insert operation in case of any password updates.

```python pgpassword.py show```

Show command lists the locally cached password entries for the host and user.

Execution just python pgpassword.py also executes the show command to list the cached password entries. There is no way of caching the passwords for the same host and user again, the addition process will fail saying **The password is already cached**. During the password caching process, users can press Ctrl+z to cancel the operation.

## Executing the pgsql utility

pgsql utility gets the parameters and internally calls the psql utility. This utility parses the arguments passed by the user to e.g:python pgsql.py args and checks for whether the password for given user id on a given host is cached locally. If yes, it sets the password by reading and decrypting it internally so that the password will not be prompt to the user and executes the psql command, else it will error out with the exit code 1 saying the password is not cached.

Example:

```python pgsql.py -h host -U user -p port```
